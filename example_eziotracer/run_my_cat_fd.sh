#/bin/bash

input_file=my_cat_fd
output_file=output_file

cmd="./my_cat_fd $input_file $output_file"
if [ $# -gt 0 ]; then
    cmd=$@
fi

echo "running:"
echo "$cmd"
$cmd
