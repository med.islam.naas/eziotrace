#!/bin/bash


EZTRACE=$(which eztrace)

data_dir=data_dir
export IOTRACER_TRACED_INODE=$(ls -id $data_dir |cut -f1 -d" ")
export IOTRACER_TRACED_FILTER=dir
export IOTRACER_STATE=ENABLED
env_variables="IOTRACER_TRACED_INODE=$IOTRACER_TRACED_INODE IOTRACER_TRACED_FILTER=$IOTRACER_TRACED_FILTER IOTRACER_STATE=$IOTRACER_STATE"

input_file=data_dir/file_10k
output_file=$data_dir/output_file

cmd="./my_cat_fd $input_file $output_file"
if [ $# -gt 0 ]; then
    cmd=$@
else
    make my_cat_fd
fi


echo "running:"
echo "sudo $env_variables eztrace -t posixio $cmd"
sudo $env_variables  $EZTRACE -t posixio $cmd



eztrace_trace=$(echo $cmd|awk '{print $1}')_trace/eztrace_log.otf2
iotracer_trace=IOtracer_output/log.otf2 
# merge traces
eta_merge  -m "$eztrace_trace"  "$iotracer_trace" 
