# Example of the IDIOM  tool chain

## Example program
my_cat_fd.c opens an input file and an output file. It reads one byte from the input file, and write it to the output file, and repeats until the end of the input file is reached.

example:

```
$ ./run_vanilla.sh
make: 'my_cat_fd' is up to date.
running:
./my_cat_fd data_dir/file_10k data_dir/output_file
10240 bytes copied in 5964659.000000 usec -> 0.001717 MB/s
-> 291.243115 usec/ function call
```

## Installing the tool chain
Please refer to https://gitlab.com/idiom1/eziotrace

## Running EZTrace
Once EZTrace is installed, run the application with eztrace:

```
$ ./run_eztrace.sh
running:
eztrace -t posixio ./my_cat_fd data_dir/file_10k data_dir/output_file

[P0T0] Starting EZTrace (pid: 850848)...
[...]
10240 bytes copied in 6163754.000000 usec -> 0.001661 MB/s
-> 300.964551 usec/ function call
[P0T0] Stopping EZTrace (pid:850848)...
```

This generates an OTF2 trace in `my_cat_fd_trace`.

You can print its content with otf2-print:

```
$ otf2-print my_cat_fd_trace/eztrace_log.otf2
=== OTF2-PRINT ===
=== Events =====================================================================
Event                               Location            Timestamp  Attributes
--------------------------------------------------------------------------------
ENTER                                      0     1969308447950111  Region: "Working" <0>
ENTER                                      0     1969308448204780  Region: "open" <5>
[...]
```

You can allow visualize the trace with a trace visualization tool such
as [ViTE](https://gitlab.inria.fr/solverstack/vite) (if it is compiled
with OTF2 support).

## Running IOTracer


```
./run_iotracer.sh 
running:
sudo /home/ftrahay/Soft/eziotrace/src/iotracer/iotracer.sh -d data_dir ./my_cat_fd data_dir/file_10k data_dir/output_file
[sudo] password for ftrahay: 
# Running python /home/ftrahay/Soft/eziotrace/src/iotracer/bcc_iotracer.py -t my_cat_fd -d -i 42739561 -l vpfb > iotracer_trace.txt &
# Running command ./my_cat_fd data_dir/file_10k data_dir/output_file
10240 bytes copied in 6655123.000000 usec -> 0.001539 MB/s
-> 324.957178 usec/ function call
# Killing IOTracer daemon 854124
# Nb events: 92060 iotracer_trace.txt
```


This generates `iotracer_trace.txt` which is a text file:

```
#timestamp level op address size probe label pid tid comm inode inodep
1969556979998360        V       R       0       0       9       L       854187  854187  my_cat_fd       0       0
1969556980201479        F       R       0       0       5       L       854187  854187  my_cat_fd       0       0
1969556980538644        V       R       0       1       2       E       854187  854187  my_cat_fd       42739570        42739561
1969556980552500        F       R       0       1       5       E       854187  854187  my_cat_fd       42739570        42739561
1969556980566133        V       R       0       1       9       E       854187  854187  my_cat_fd       42739570        42739561
```

The columns correspond to:
* `timestamp`: timestamp of the event
* `level`: which probe generated the event (`V`=VFS, `F`=FS, `P`=Page cache, `B`=Block)
* `op`: read (`R`) or write (`W`)
* `address`: offset at which the IO happened
* `size`:  number of bytes read/written
* `probe`: ?
* `label`: beginning (`E`) or end (`L`) of the I/O event
* `pid`:  PID of the process that issued the I/O 
* `tid`:  TID of the thread that issued the I/O 
* `comm`: command that issued the I/O 
* `inode`: inode of the file
* `inodep`: ?



## Running EZIOTRacer

```
./run_eziotracer.sh
make: 'my_cat_fd' is up to date.
running:
sudo IOTRACER_TRACED_INODE=42739561 IOTRACER_TRACED_FILTER=dir IOTRACER_STATE=ENABLED eztrace -t posixio ./my_cat_fd data_dir/file_10k data_dir/output_file
trace_dir: ./my_cat_fd_trace
[P0T0] Starting EZTrace (pid: 862118)...
[P0T0] IOtracer: start
[P0T0] IOtracer: converting IOtracer log file (iotracer_trace.txt) to otf2 format (IOtracer_output)
[OTF2] src/otf2_archive_int.c:3945: error: File does already exist: Could not create archive trace directory!
[OTF2] src/otf2_archive_int.c:1108: error: File does already exist: Couldn't create directories on root.
[P0T0] New function: VFS_WRITE (id=0)
[P0T0] New function: VFS_READ (id=1)
[P0T0] New function: FS_WRITE (id=2)
[P0T0] New function: FS_READ (id=3)
[P0T0] New function: BLK_WRITE (id=4)
[P0T0] New function: BLK_READ (id=5)
[...]
```

This generates several traces:

* `my_cat_fd_trace` contains the EZTrace OTF2 trace
* `IOtracer_output` contains the IOTracer OTF2 trace
* `iotracer_trace.txt` contains the IOTracer text trace
* `output` contains the merge OTF2 trace (that contains both eztrace and iotracer events)
